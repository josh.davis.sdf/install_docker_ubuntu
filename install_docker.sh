#!/bin/sh
#ref: https://docs.docker.com/engine/install/ubuntu/

#remove old versions
apt-get remove docker docker-engine docker.io containerd runc 

#setup repo
apt-get install -y \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg-agent \
    software-properties-common

#add docker's gpg key
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | apt-key add -
apt-key fingerprint 0EBFCD88


 #stable repo for ubuntu
add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
   $(lsb_release -cs) \
   stable" 

#install from repo
apt-get update
apt-get install -y docker-ce docker-ce-cli containerd.io


#ref: https://docs.docker.com/engine/install/linux-postinstall/
#enable non-sudo use
groupadd docker
usermod -aG docker $USER
systemctl enable docker